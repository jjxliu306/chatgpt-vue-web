import request from '@/utils/request'

export function getHistoryMsg(data) {
  return request({
    url: '/chat/getHistoryMsg',
    method: 'post',
    data
  })
}
